package com.abzooba.pdf;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Map;
import java.util.HashMap;

public class PayslipFormatDetector {
	private static String parentFolderName;
	private static String outputFolderName;

	private static Map<String, String> filenamesAndFormats = new HashMap<String, String>();
	/**
	 * 
	 * main method
	 * 
	 */
	public static void main(String args[]) {
		try {
			filenamesAndFormats.put("APPELF__1548-000000009034670-15482891_&I100PYSTB_20191016175532_40000145-09034670-15482891-001_6_.pdf", "Sample5");
			filenamesAndFormats.put("APPELF__1647-000000009015160-16473011_&I100PYSTB_20191028221256_49000910-09015160-16473011-001_5_.pdf", "Sample5");
			filenamesAndFormats.put("APPELF__2606-000000009029290-26062901_&I100PYSTB_20191017224756_05500280-09029290-26062901-001_4_.pdf", "Sample6");
			filenamesAndFormats.put("APPELF__2606-000000009029290-26062901_&I100PYSTB_20191017224756_05500280-09029290-26062901-003_3_.pdf", "Sample6");
			filenamesAndFormats.put("APPELF__2925-000000009020940-29253031_&I100PYSTB_20191030161221_13000603-09020940-29253031-001_27_.pdf", "Sample7");
			filenamesAndFormats.put("APPELF__3000-000000009020920-30002841_&I100PYSTB_20191011083817_33000242-09020920-30002841-001_45_.pdf", "Sample7");
			filenamesAndFormats.put("APPELF__3749-000000009038590-37492811_&I100PYSTB_20191008155046_09800050-09038590-37492811-001_30_.pdf", "Sample9");
			filenamesAndFormats.put("APPELF__3749-000000009038590-37492811_&I100PYSTB_20191016234556_09800050-09038590-37492811-002_13_.pdf", "Sample9");
			PayslipFormatDetector formatDetector = new PayslipFormatDetector();
			if (args.length < 2) {
				System.out
						.println("Usage: com.abzooba.pdf.PayslipFormatDetector <input_folder> <output_folder>)");
				System.exit(-1);
			}
			parentFolderName = args[0];
			outputFolderName = args[1];
			
			File outputFile = new File(outputFolderName);
			if (!outputFile.exists()) {
				outputFile.mkdirs();
			}
			formatDetector.processDocuments();
		}

		catch (Exception exc) {
			exc.printStackTrace();
		}

	}

	/**
	 * 
	 * processes documents
	 * 
	 */
	protected void processDocuments() throws FileNotFoundException{

		// loop through folder, and process each file
		if (parentFolderName != null && !parentFolderName.isEmpty()) {
			File parentFolder = new File(parentFolderName);
			if (parentFolder.isDirectory()) {
				File[] fileList = parentFolder.listFiles();
				System.err.println("Parent Folder:" + parentFolder.getName());
				System.err.println("No. of files: " + fileList.length);
				for (File file : fileList) {
					System.err.println("Checking file " + file.getName());
					if (file.isFile() 
							&& (file.getAbsolutePath().endsWith(".pdf"))){
						System.err.println("Processing started for : " + file.getName());
						try {
							String format = detectFormat (file);
							System.err.println("Detected format: " + format);
							// create  folder in output folder with name <format>
							// and copy file into it
							File outputFolder = new File(outputFolderName + File.separator + format);
							if (!outputFolder.exists())
								outputFolder.mkdirs();
							// copy the file to output dir
							File destFile = new File(outputFolder, file.getName());
							FileReader r = new FileReader (file);
							FileWriter w = new FileWriter (destFile);
							char c[] = new char[1024];
							while (r.read(c) >= 0) {
								w.write(c);
							}
							r.close();
							w.close();
							Thread.sleep(1000);
								
							
							
							
						}

						catch (Exception exc) {
						}

					}
					System.err.println("Processing finished for : "+file.getName());
				}
			}
		}

	}
	
	/**
	 * 
	 * detects format of the specified document
	 * 
	 * @param document document whose format is to be detected
	 * @return format (string)
	 * 
	 */
	public String detectFormat (File file) {
		String format;
		if ((format = filenamesAndFormats.get(file.getName())) != null)
			return format;
		
		return "Unknown";
	}

}

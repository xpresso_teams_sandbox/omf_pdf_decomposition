"""Exploration on date attribute"""
__all__ = ["DistributedExploreDate"]
__author__ = ["Sanyog Vyawahare"]

from datetime import timedelta

import databricks.koalas as ks


class DistributedExploreDate():
    """Class for date analysis"""

    def __init__(self, data):
        self.data = data
        return

    def populate_date(self):
        """Sets the metrics for date type attribute"""
        resp = dict()
        min_date, max_date, day_count, month_count, year_count, missing_dates \
            = \
            self.date_analysis(self.data)
        resp["min"] = min_date
        resp["max"] = max_date
        resp["day_count"] = day_count
        resp["month_count"] = month_count
        resp["year_count"] = year_count
        resp["missing_dates"] = missing_dates
        return resp

    @staticmethod
    def date_analysis(data):
        """Performs analysis on date attribute values
        Args:
            data (:obj:`list`): records of date attribute
        Returns:
            min (:obj:`Timestamp`): Minimum value of date
            max (:obj:`Timestamp`): Maximum value of date
            day_count, (`int`): Count of number of days
            month_count, (`int`): Count of number of month
            year_count, (`int`): Count of number of years
            missing_date_tuple (:obj:`list`): list of tuples of missing date
            ranges
            """
        data = ks.to_datetime(data, infer_datetime_format=True)
        minimum = data.min().strftime("%d-%m-%Y")
        maximum = data.max().strftime("%d-%m-%Y")
        day_count = data.dt.day_name().dropna().value_counts()
        day_count = day_count.to_dict()
        month_count = data.dt.month_name().dropna().value_counts()
        month_count = month_count.to_dict()
        year_count = data.dt.year.dropna().value_counts()
        year_count = year_count.to_dict()
        dates = ks.to_datetime(data, infer_datetime_format=True)
        dates = dates.loc[dates.notnull()]
        dates = dates.unique()
        dates = dates.to_list()
        unique_dates = sorted(dates)
        missing_date_tuple = [((previous + timedelta(1)).strftime("%d-%m-%Y"),
                               (current - timedelta(1)).strftime("%d-%m-%Y"))
                              for previous, current in
                              zip(unique_dates[:-1], unique_dates[1:])
                              if current != previous + timedelta(1)]
        return minimum, maximum, day_count, month_count, year_count, missing_date_tuple
